#include <nrfx.h>
#include <nrfx_config.h>
#include <nrf.h>
#include <nrf_gpio.h>
#include <nrfx_systick.h>
#include <stdint.h>

#define LED NRF_GPIO_PIN_MAP(0,28)
 
int main(void)
{ 
  nrf_gpio_cfg_output(LED);
  nrfx_systick_init();

  while (1) {
    nrf_gpio_pin_set(LED);
    nrfx_systick_delay_ms(500);
    nrf_gpio_pin_clear(LED);
    nrfx_systick_delay_ms(100);
  }
} 
