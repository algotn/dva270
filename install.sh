#!/bin/sh
#git clone --branch v3.1.0 https://github.com/NordicSemiconductor/nrfx.git
#git clone --branch 5.9.0 https://github.com/ARM-software/CMSIS_5.github

cd ..
mkdir -p nrfx/CMSIS/Core
cp -r CMSIS_5/CMSIS/Core nrfx/CMSIS/Core

cd nrfx
mkdir -p src/sample
mkdir build

cp templates/nrfx_config.h src/sample/
cp templates/nrfx_config_nrf5340_application.h src/sample/
cp templates/nrfx_config_common.h src/
cp templates/nrfx_glue.h src/
cp templates/nrfx_log.h src/
