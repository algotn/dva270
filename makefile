# Build Nordic NRF5340DK_CPUAPP example
# Definera filnamn och bestäm relativa paths

TOP		 := $(CURDIR)/..
COMPILER	 := $(TOP)/arm-gnu-toolchain-13.2.Rel1-x86_64-arm-none-eabi/bin/arm-none-eabi-
TARGET           := $(shell basename $(CURDIR)).elf
HEXFILE          := $(TARGET:.elf=.hex)
BUILD_DIR        := $(CURDIR)/build
NRFX             := $(TOP)/nrfx
LINKER_SCRIPT    := nrf5340_xxaa_application.ld
# Programmen från arm-toolchain vi kommer använda. De är egentligen .exe filer bara att Windows vet vart dom ligger så vi kan kalla
# dem utan .exe biten eller speficikt behöva peka på dom.
GCC              := $(COMPILER)gcc
OBJCOPY          := $(COMPILER)objcopy
OBJSIZE          := $(COMPILER)size
# Väldigt generöst tilldelad mängd minne så vi inte behöver oroa oss över det i denna kurs:
HEAP_SIZE        := 16384
STACK_SIZE       := 16384
# Source filer vi behöver. De första 2 är krav och definerar variabler specifika för vårt kort.
SRC_FILES        += $(NRFX)/mdk/gcc_startup_nrf5340_application.S
SRC_FILES        += $(NRFX)/mdk/system_nrf5340_application.c
SRC_FILES        += $(NRFX)/drivers/src/nrfx_systick.c
SRC_FILES        += main.c
# Här kan ni länka till extra source filer ni vill använda.
# SRC_FILES        += inte_en_riktigt_fil.c
# Definera folders
SRC_DIRS         := $(dir $(SRC_FILES))
SRCES            := $(notdir $(SRC_FILES))
OBJECTS          := $(SRCES:=.o)
OBJ_FILES        := $(addprefix $(BUILD_DIR)/,$(OBJECTS))

### Include header file directories (using the default nfrx folder structure)
INC_FOLDERS      += $(TOP)/arm-gnu-toolchain-13.2.Rel1-x86_64-arm-none-eabi/arm-none-eabi/include
INC_FOLDERS      += $(NRFX)/hal
INC_FOLDERS      += $(NRFX)/soc
INC_FOLDERS      += $(NRFX)/haly
INC_FOLDERS      += $(NRFX)/drivers/
INC_FOLDERS      += $(NRFX)/drivers/include
INC_FOLDERS      += $(NRFX)/drivers/prs
INC_FOLDERS      += $(NRFX)/CMSIS/Core/Include
INC_FOLDERS      += $(NRFX)/mdk
INC_FOLDERS      += $(NRFX)/src
INC_FOLDERS      += $(CURDIR)
INC_FOLDERS      += $(NRFX)
INC_FOLDERS      += $(NRFX)/helpers
CFLAGS           += -std=c99 -g -MP -MD -c -g3 $(OPTIMISE)
CFLAGS           += -DBOARD_CUSTOM -DBSP_DEFINES_ONLY -DCONFIG_GPIO_AS_PINRESET -DFLOAT_ABI_HARD -DNRF5340_XXAA_APPLICATION
CFLAGS           += -mcpu=cortex-m33 -mthumb -mabi=aapcs -Wall -mfloat-abi=hard -mfpu=fpv5-sp-d16 -ffunction-sections -fdata-sections
CFLAGS           += -fno-strict-aliasing -fno-builtin -fshort-enums -D__HEAP_SIZE=$(HEAP_SIZE) -D__STACK_SIZE=$(STACK_SIZE)

LNFLAGS          += -O3 -g3 -mthumb -mabi=aapcs -L$(NRFX)/mdk -T$(LINKER_SCRIPT) -mcpu=cortex-m33 -mfloat-abi=hard -mfpu=fpv5-sp-d16
LNFLAGS          += -Wl,--gc-sections  --specs=nano.specs
LNFLAGS2         += -Wl,-Map=$(CURDIR)/build/mymap.map -lc -lnosys -lm

VPATH            := $(SRC_DIRS)

all: $(BUILD_DIR)/$(TARGET)

$(BUILD_DIR):
	@mkdir $(BUILD_DIR)

$(BUILD_DIR)/$(TARGET): $(OBJ_FILES)
	@echo Linking $@
	$(GCC) $(LNFLAGS) -o $@ $^ $(LNFLAGS2)
	@$(OBJCOPY) -O ihex $(BUILD_DIR)/$(TARGET) $(BUILD_DIR)/$(HEXFILE)
	@$(OBJCOPY) -O binary $(BUILD_DIR)/$(TARGET) $(BUILD_DIR)/$(TARGET:.elf=.bin)
	@$(OBJSIZE) $(BUILD_DIR)/$(TARGET)

$(BUILD_DIR)/%.c.o: %.c $(BUILD_DIR)
	@echo Compiling $<
	@$(GCC) $(CFLAGS) -o $@ $< $(addprefix -I,$(INC_FOLDERS))

$(BUILD_DIR)/%.S.o: %.S $(BUILD_DIR)
	@echo Assembling $<
	@$(GCC) $(CFLAGS) -o $@ $<

jflash: $(BUILD_DIR)/$(TARGET)
	@echo Flashing $(HEXFILE)
	nrfjprog --program $(BUILD_DIR)/$(HEXFILE) --sectorerase --verify
	nrfjprog --reset

clean:
	@echo cleaning build directory
	rm -rf $(BUILD_DIR)
   
gdbserver:
	JLinkGDBServer -select USB=0 -device nRF5340_xxAA_APP -endian little -if SWD -speed auto -noir -LocalhostOnly -nologtofile -port 2331 -SWOPort 2332 -TelnetPort 2333

gdb: $(BUILD_DIR)/$(TARGET)
	gdb $(BUILD_DIR)/$(TARGET) -x gdbconf
